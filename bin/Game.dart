import 'dart:io';

import 'Bakery.dart';
import 'Fruit.dart';
import 'Order.dart';
import 'Appliance.dart';
import 'Vagetable.dart';
import 'Water.dart';

class Game {
  late String name;
  var zone;
  var listMenu = [];
  int point = 0;

  Appliance app = new Appliance();
  Water water = new Water();
  Bakery bake = new Bakery();
  Fruit fruit = new Fruit();
  Vegetable veget = new Vegetable();
  Order order = new Order();

  void showWelcome() {
    print(
        // ignore: prefer_single_quotes
        r"""\
                ░██╗░░░░░░░██╗███████╗██╗░░░░░░█████╗░░█████╗░███╗░░░███╗███████╗
                ░██║░░██╗░░██║██╔════╝██║░░░░░██╔══██╗██╔══██╗████╗░████║██╔════╝
                ░╚██╗████╗██╔╝█████╗░░██║░░░░░██║░░╚═╝██║░░██║██╔████╔██║█████╗░░
                ░░████╔═████║░██╔══╝░░██║░░░░░██║░░██╗██║░░██║██║╚██╔╝██║██╔══╝░░
                ░░╚██╔╝░╚██╔╝░███████╗███████╗╚█████╔╝╚█████╔╝██║░╚═╝░██║███████╗
                ░░░╚═╝░░░╚═╝░░╚══════╝╚══════╝░╚════╝░░╚════╝░╚═╝░░░░░╚═╝╚══════╝

░██████╗██╗░░░██╗██████╗░███████╗██████╗░███╗░░░███╗░█████╗░██████╗░██╗░░██╗███████╗████████╗
██╔════╝██║░░░██║██╔══██╗██╔════╝██╔══██╗████╗░████║██╔══██╗██╔══██╗██║░██╔╝██╔════╝╚══██╔══╝
╚█████╗░██║░░░██║██████╔╝█████╗░░██████╔╝██╔████╔██║███████║██████╔╝█████═╝░█████╗░░░░░██║░░░
░╚═══██╗██║░░░██║██╔═══╝░██╔══╝░░██╔══██╗██║╚██╔╝██║██╔══██║██╔══██╗██╔═██╗░██╔══╝░░░░░██║░░░
██████╔╝╚██████╔╝██║░░░░░███████╗██║░░██║██║░╚═╝░██║██║░░██║██║░░██║██║░╚██╗███████╗░░░██║░░░
╚═════╝░░╚═════╝░╚═╝░░░░░╚══════╝╚═╝░░╚═╝╚═╝░░░░░╚═╝╚═╝░░╚═╝╚═╝░░╚═╝╚═╝░░╚═╝╚══════╝░░░╚═╝░░░

                            ░██████╗░░█████╗░███╗░░░███╗███████╗
                            ██╔════╝░██╔══██╗████╗░████║██╔════╝
                            ██║░░██╗░███████║██╔████╔██║█████╗░░
                            ██║░░╚██╗██╔══██║██║╚██╔╝██║██╔══╝░░
                            ╚██████╔╝██║░░██║██║░╚═╝░██║███████╗
                            ░╚═════╝░╚═╝░░╚═╝╚═╝░░░░░╚═╝╚══════╝

                """);
  }

  void getName() {
    print(" ***Hello, welcome to the game SuperMartket***\nWhat your name?");
    stdout.write("My name is : ");
    var name1 = stdin.readLineSync()!;
    name = name1;
  }

  void showWelZone() {
    print("\n»»------Go to the Supermarket------»»\n");
    print("@At The Supermartket");
  }

  void showZone() {
    print("\n*** Select the zone you want to go ***\n 0.Exit Zone \n 1.Appliance\n 2.Water\n 3.Bakery\n 4.Friut\n 5.Vegetable");
    stdout.write("Choosing is :");
    zone = stdin.readLineSync()!;
  }

  void showListGame() {
    stdout.write("List Game : ");
    order.getOrderList();
    stdout.write("My List Order : ");
    print(listMenu);
  }

  void checkZone() {
    switch (zone) {
      case '1':
        print("\n-At Zone Appliance-");
        app.showMenu();
        showListGame();
        addMenuApp();
        break;
      case '2':
        print("\n-At Zone Water-");
        water.showMenu();
        showListGame();
        addMenuWater();
        break;
      case '3':
        print("\n-At Zone Bakery-");
        bake.showMenu();
        showListGame();
        addMenuBake();
        break;
      case '4':
        print("\n-At Zone Fruit-");
        fruit.showMenu();
        showListGame();
        addMenuFruit();
        break;
      case '5':
        print("\n-At Zone Vegetable-");
        veget.showMenu();
        showListGame();
        addMenuVegetable();
        break;
      case '0':
        print("Exit Zone");
        showListGame();
        break;

      default:
        break;
    }
  }

  void addMenuApp() {
    stdout.write("Choosing is : ");
    int? menu = int.parse(stdin.readLineSync()!);
    listMenu.add(app.zoneApp[menu - 1]);
    showZone();
    checkZone();
  }

  void addMenuWater() {
    stdout.write("Choosing is : ");
    int? menu = int.parse(stdin.readLineSync()!);
    listMenu.add(water.zoneWater[menu - 1]);
    showZone();
    checkZone();
  }

  void addMenuBake() {
    stdout.write("Choosing is : ");
    int? menu = int.parse(stdin.readLineSync()!);
    listMenu.add(bake.zoneBake[menu - 1]);
    showZone();
    checkZone();
  }

  void addMenuFruit() {
    stdout.write("Choosing is : ");
    int? menu = int.parse(stdin.readLineSync()!);
    listMenu.add(fruit.zoneFruit[menu - 1]);
    showZone();
    checkZone();
  }

  void addMenuVegetable() {
    stdout.write("Choosing is : ");
    int? menu = int.parse(stdin.readLineSync()!);
    listMenu.add(veget.zoneVeget[menu - 1]);
    showZone();
    checkZone();
  }

  void checkPoint() {
    try {
      for (int i = 0; i < order.listOrder.length; i++) {
        for (int j = 0; j < listMenu.length; j++) {
          if (order.listOrder[i] == listMenu[j]) {
            point++;
          } else {
            point+0;
          }
        }
      }
    } catch (e) {}
  }

  void Ques(){
    stdout.write("\n༶•┈┈┈┈┈┈ Do you want play again ? (y or n) ┈┈┈┈┈•༶ : ");
    var ans = stdin.readLineSync()!;
    if(ans == 'y'){
      print("---- You want play again ----");
      startGame();
    }else{
      stdout.write("╚═══════════════ °❀•° Bye bye $name °•❀°═══════════════╝");
    }
  }

  void startGame() {
    showWelcome();
    getName();
    order.showFamily();
    order.checkFamily();
    showWelZone();
    showZone();
    checkZone();
    checkPoint();
    stdout.write("Hey! $name, your score is : $point point\n");
    //print(point);
    Ques();
  }
}
