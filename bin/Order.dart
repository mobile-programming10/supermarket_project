import 'dart:io';

class Order {
  late int family;
  var listOrder = [];
  var listChef = [
    'knife',
    'bowl',
    'honey milky',
    'lamon soda',
    'brownie matcha latte',
    'strawberry pie',
    'strawberry',
    'kiwi',
    'raddicchio orgranic',
    'water spinach',
    'coriander'
  ];
  var listGardener = [
    'rubber hose',
    'broom',
    'Hales Blue Boy',
    'crapecake',
    'pickled mango',
    'orange',
    'tomato',
    'papaya'
  ];
  var listMaid = [
    'broom',
    'apron',
    'pepsi',
    'coconut cake',
    'grap',
    'mesclun orgranic',
    'papaya',
    'bell pepper'
  ];

  void showFamily() {
    print(
        "***Select the list of items you are interested in***\n 1.Chef's family\n 2.Garden's family\n 3.Maid's family");
  }

  void checkFamily() {
    stdout.write("Choosing is : ");
    var fam = stdin.readLineSync()!;

    switch (fam) {
      case '1':
        listOrder = listChef;
        print("chooseing is Chef's family\n");
        print("The Chef's Family Must-Buy Items :");
        try {
          for (int i = 1; i <= listChef.length; i++) {
            print(' $i.' + listChef[i - 1]);
          }
        } catch (e) {}
        break;
      case '2':
        listOrder = listGardener;
        print("chooseing is Garden's family\n");
        print("The Garden's Family Must-Buy Items :");
        try {
          for (int i = 1; i <= listGardener.length; i++) {
            print(' $i.' + listGardener[i - 1]);
          }
        } catch (e) {}
        break;
      case '3':
        listOrder = listMaid;
        print("chooseing is Maid's family\n");
        print("The Maid's Family Must-Buy Items :");
        try {
          for (int i = 1; i <= listMaid.length; i++) {
            print(' $i.' + listMaid[i - 1]);
          }
        } catch (e) {}
        break;
      default:
        checkFamily();
        break;
    }
  }

  void getOrderList() {
    this.listOrder = listOrder;
    print(listOrder);
  }
}
