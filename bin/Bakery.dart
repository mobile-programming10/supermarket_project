import 'dart:io';
import 'Supermarket.dart';

class Bakery extends Supermarket {
  var zoneBake = [
    'brownie matcha latte',
    'brownie oreo',
    'brownie chocolate',
    'coconut cake',
    'rainbow cake',
    'red velvet cake',
    'cookie mushmellow',
    'cookie chocolate chip',
    'strawberry pie',
    'crapecake',
    'bread',
    'french baquette'
  ];

  @override
  void showMenu() {
    try {
      for (int i = 1; i <= zoneBake.length; i++) {
        print(' $i.' + zoneBake[i - 1]);
      }
    } catch (e) {}
  }
}
