import 'dart:io';
import 'Supermarket.dart';

class Appliance extends Supermarket {
  var zoneApp = [
    'knife',
    'turner',
    'pot',
    'bowl',
    'rubber hose',
    'broom',
    'apron',
    'blender',
    'toaster',
    'hair dryer'
  ];

  @override
  void showMenu() {
    try {
      for (int i = 1; i <= zoneApp.length; i++) {
        print(' $i.' + zoneApp[i - 1]);
      }
    } catch (e) {}
  }
}
